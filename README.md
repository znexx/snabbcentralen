Snabbcentralen
==============

Detta är ett skript som används för att förbättra funktionaliteten för operatörer på Chattcentralen.se.

**Detta är originalet och inte något av tredjepartsvarianterna som finns på exv. pastebin.com**

Jag håller på och gå igenom vilka förbättringar som finns bland de andra versionerna av detta skript, så saknar ni någon funktionalitet därifrån i min variant så släng över ett meddelande om vad det är ni saknar så fixas det så småningom. Har ni idéer till nya förbättringar eller saker som borde vara annorlunda så tveka inte att höra av er till mig på **znexxpwnz0r snabel-a gmail punkt com**!

För utvecklare: jag är öppen för samarbeten så sitt inte och copypasta skiten till nån jävla pastebin, utan forka och gör pull request här på bitbucket i stället. Är ni tillräckligt trevliga kanske jag till och med ger er permissions direkt på detta repositoryt.

Installation
------------

Att installera och börja använda skriptet görs med tre snabba och enkla steg:

1. Gå in på [Tampermonkeys hemsida](https://tampermonkey.net/) och installera pluginet för din webbläsare
2. Klicka på länken till skriptet [här](https://kattm.at/snabbcentralen/snabbcentralen.user.js)
3. Tampermonkey kommer visa dig koden för skriptet och ger dig några val att göra. Klicka på knappen som heter "Install"

Nu är skriptet installerat och färdigt att använda! Notera att det har skapats en extra knapp på chattcentralen-sidan som heter "Settings". Här kan du göra de inställningar som passar dig personligen!

Uppdateringar kommer ske automatiskt, och vilken version du använder och vilken som är den senaste ser du överst i fönstret med inställningar.

För mer info och en riktig hemsida, se [https://kattm.at/snabbcentralen/](https://kattm.at/snabbcentralen/).
